#pragma once

#include <Eigen/Core>
#include <functional>

namespace hw {

using RecalcFuncType = std::function<Eigen::VectorXd(double, const Eigen::VectorXd &)>;
using ResultFuncType = std::function<Eigen::VectorXd(double)>;

constexpr double DEFAULT_STEP = 0.1;
constexpr double DIFF_STEP    = 0.001;

struct SolverOptions {
    double step;

    double max_diff;
    double min_diff;

    double start_time;

    Eigen::VectorXd start_values;
};

class Solver {
 public:
    virtual ~Solver() = default;

    virtual void Solve() = 0;
};

class RungeSolver : public Solver {
 public:
    struct Result {
        double max_diff;
        double step;
    };

 public:
    RungeSolver(RecalcFuncType &recalc_func, ResultFuncType &result_func, const SolverOptions &opt)
        : _recalc_func(recalc_func), _result_func(result_func), _time(opt.start_time), _step(opt.step),
          _values(opt.start_values), _max_diff(opt.max_diff), _min_diff(opt.min_diff) {
        InitButcher();
    };

    void Solve() override;

    Result Result() { return _res; };

 private:
    void CalcStep();
    Eigen::MatrixXd CreateTmpMat();
    void InitButcher();

 private:
    RecalcFuncType _recalc_func;
    ResultFuncType _result_func;
    double _max_diff;
    double _min_diff;

    double _time;
    double _step;
    Eigen::VectorXd _values;

    struct Result _res;

 private:
    static constexpr double MAX_TIME = 0.2;
    Eigen::VectorXd STEPS;
    Eigen::MatrixXd B;
    Eigen::MatrixXd A;
};

class DPSolver : public Solver {
 public:
    struct Result {
        double min_step;
        int total_step;
    };

 public:
    DPSolver(RecalcFuncType &recalc_func, ResultFuncType &result_func, const SolverOptions &opt)
        : _recalc_func(recalc_func), _result_func(result_func), _time(opt.start_time), _step(opt.step),
          _values(opt.start_values), _max_diff(opt.max_diff), _min_diff(opt.min_diff) {
        InitButcher();
    };

    void Solve() override;

    Result Result() { return _res; }

 private:
    void CalcStep();
    Eigen::MatrixXd CreateTmpMat();
    void InitButcher();

 private:
    RecalcFuncType _recalc_func;
    ResultFuncType _result_func;
    double _max_diff;
    double _min_diff;

    double _time;
    double _step;
    Eigen::VectorXd _values;

    struct Result _res;

 private:
    static constexpr double MAX_TIME = 10;
    Eigen::VectorXd STEPS;
    std::vector<Eigen::MatrixXd> B;
    Eigen::MatrixXd A;
};
}  // namespace hw