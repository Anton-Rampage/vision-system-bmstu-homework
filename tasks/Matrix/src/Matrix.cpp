#include "Matrix.h"

#include <Exception.h>
#include <iomanip>
#include <limits>
#include <ostream>

namespace hw {
Matrix::Matrix(uint32_t rows, uint32_t cols) {
    if (rows == 0 || cols == 0) {
        throw Exception("nul size matrix");
    }
    _rows = rows;
    _cols = cols;
    _data.resize(_rows);
    for (auto& row : _data) {
        row.resize(_cols);
    }
}

double Matrix::operator()(uint32_t i, uint32_t j) const {
    if (i >= _rows || j >= _cols) {
        throw Exception("bad i or j");
    }
    return _data[i][j];
}

double& Matrix::operator()(uint32_t i, uint32_t j) {
    if (i >= _rows || j >= _cols) {
        throw Exception("bad i or j");
    }
    return _data[i][j];
}

Matrix::Matrix(const std::vector<std::vector<double>>& mat) {
    if (mat.empty()) {
        throw Exception("empty matrix");
    }
    if (mat[0].empty()) {
        throw Exception("empty row");
    }
    _rows = mat.size();
    _cols = mat[0].size();
    _data = mat;
}

uint32_t Matrix::cols() const { return _cols; }

uint32_t Matrix::rows() const { return _rows; }

Matrix Matrix::operator*(double val) const {
    Matrix new_mat(_rows, _cols);
    for (size_t i = 0; i < _rows; ++i) {
        for (size_t j = 0; j < _cols; ++j) {
            new_mat(i, j) = (*this)(i, j) * val;
        }
    }
    return new_mat;
}

Matrix Matrix::T() const {
    Matrix t_mat(_cols, _rows);

    for (size_t i = 0; i < t_mat.rows(); ++i) {
        for (size_t j = 0; j < t_mat.cols(); ++j) {
            t_mat(i, j) = (*this)(j, i);
        }
    }
    return t_mat;
}

bool Matrix::operator==(const Matrix& mat) const {
    if (_rows != mat.rows() || _cols != mat.cols()) {
        return false;
    }
    static constexpr double EPS = 1e-7;
    for (size_t i = 0; i < mat.rows(); ++i) {
        for (size_t j = 0; j < mat.cols(); ++j) {
            if (std::abs(mat(i, j) - (*this)(i, j)) > EPS) {
                return false;
            }
        }
    }

    return true;
}

bool Matrix::operator!=(const Matrix& mat) const { return !(*this == mat); }

Matrix Matrix::dot(const Matrix& mat) const {
    if (_cols != mat.rows()) {
        throw Exception("not correct sizes of matrix");
    }

    Matrix matrix(_rows, mat.cols());

    for (size_t i = 0; i < matrix.rows(); ++i) {
        for (size_t j = 0; j < matrix.cols(); ++j) {
            for (size_t k = 0; k < _cols; ++k) {
                matrix(i, j) += (*this)(i, k) * mat(k, j);
            }
        }
    }
    return matrix;
}

Matrix Matrix::operator+(const Matrix& mat) const {
    if (_rows != mat.rows() || _cols != mat.cols()) {
        throw Exception("not correct sizes of matrix");
    }

    Matrix matrix(_rows, _cols);

    for (size_t i = 0; i < matrix.rows(); ++i) {
        for (size_t j = 0; j < matrix.cols(); ++j) {
            matrix(i, j) = (*this)(i, j) + mat(i, j);
        }
    }
    return matrix;
}

Matrix Matrix::operator-(const Matrix& mat) const { return *this + (-mat); }

Matrix Matrix::operator-() const {
    Matrix matrix(_rows, _cols);

    for (size_t i = 0; i < matrix.rows(); ++i) {
        for (size_t j = 0; j < matrix.cols(); ++j) {
            matrix(i, j) = -(*this)(i, j);
        }
    }
    return matrix;
}

double Matrix::det() const {
    if (_rows != _cols) {
        throw Exception("matrix is not square");
    }
    auto [L, U] = LU();

    double det = (*this)(0, 0);
    for (uint32_t i = 1; i < _rows; ++i) {
        det *= U(i, i);
    }

    return det;
}

std::pair<Matrix, Matrix> Matrix::LU() const {
    Matrix L(_rows, _cols);
    Matrix U(_rows, _cols);
    for (uint32_t i = 0; i < _rows; ++i) {
        for (uint32_t j = 0; j < _cols; j++) {
            if (j < i) {
                U(i, j) = 0;
            } else {
                U(i, j) = (*this)(i, j);
                for (uint32_t k = 0; k < i; k++) {
                    U(i, j) -= L(i, k) * U(k, j);
                }
            }
        }
        for (uint32_t j = 0; j < _cols; ++j) {
            if (j < i) {
                L(j, i) = 0;
            } else if (j == i) {
                L(i, j) = 1;
            } else {
                L(j, i) = (*this)(j, i) / U(i, i);
                for (uint32_t k = 0; k < i; ++k) {
                    L(j, i) -= L(j, k) * U(k, i) / U(i, i);
                }
            }
        }
    }
    return {L, U};
}

Matrix Matrix::inverse() const {
    Matrix IA(_rows, _cols);
    for (uint32_t i = 0; i < _rows; ++i) {
        IA(i, i) = 1;
    }
    auto [L, U] = LU();
    for (int j = 0; j < _rows; j++) {
        for (int i = 0; i < _cols; i++) {
            for (int k = 0; k < i; k++) {
                IA(i, j) -= L(i, k) * IA(k, j);
            }
            IA(i, j) /= L(i, i);
        }

        for (int i = _rows - 1; i >= 0; i--) {
            for (int k = i + 1; k < _rows; k++) {
                IA(i, j) -= U(i, k) * IA(k, j);
            }

            IA(i, j) /= U(i, i);
        }
    }
    return IA;
}

std::ostream& operator<<(std::ostream& out, const Matrix& matrix) {
    out.exceptions(std::ostream::failbit | std::ostream::badbit);

    out << std::setprecision(std::numeric_limits<double>::max_digits10);

    uint32_t cols = matrix.cols();
    for (size_t i = 0; i < matrix.rows(); ++i) {
        for (size_t j = 0; j < cols; ++j) {
            out << matrix(i, j);
            if (j == cols - 1) {
                out << "\n";
            } else {
                out << " ";
            }
        }
    }
    return out;
}

Matrix operator*(double val, const Matrix& matrix) { return matrix * val; }

Matrix forward(const Matrix& L, const Matrix& b) {
    Matrix y(L.rows(), 1);

    for (int i = 0; i < L.rows(); ++i) {
        y(i, 0) = b(i, 0);
        for (int j = 0; j < i; ++j) {
            y(i, 0) -= L(i, j) * y(j, 0);
        }
        y(i, 0) /= L(i, i);
    }
    return y;
}

Matrix back(const Matrix& U, const Matrix& y) {
    Matrix x(U.rows(), 1);

    for (int i = U.rows() - 1; i >= 0; --i) {
        x(i, 0) = y(i, 0);
        for (int j = i + 1; j < U.rows(); ++j) {
            x(i, 0) -= U(i, j) * x(j, 0);
        }
        x(i, 0) /= U(i, i);
    }
    return x;
}

Matrix solve(const Matrix& A, const Matrix& b) {
    auto [L, U] = A.LU();

    Matrix y = forward(L, b);
    Matrix x = back(U, y);
    return x;
}

}  // namespace hw
