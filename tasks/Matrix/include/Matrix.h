#pragma once

#include <string>
#include <vector>

namespace hw {
class Matrix {
 public:
    Matrix() = delete;
    Matrix(uint32_t rows, uint32_t cols);
    explicit Matrix(const std::vector<std::vector<double>>& mat);

    Matrix(const Matrix& mat) = default;
    Matrix& operator=(const Matrix& mat) = default;

    Matrix(Matrix&& mat) = default;
    Matrix& operator=(Matrix&& mat) = default;

    ~Matrix() = default;

    double operator()(uint32_t i, uint32_t j) const;
    double& operator()(uint32_t i, uint32_t j);
    [[nodiscard]] uint32_t cols() const;
    [[nodiscard]] uint32_t rows() const;

    [[nodiscard]] Matrix T() const;
    Matrix operator*(double val) const;
    Matrix operator+(const Matrix& mat) const;
    Matrix operator-(const Matrix& mat) const;
    Matrix operator-() const;
    [[nodiscard]] Matrix dot(const Matrix& mat) const;

    [[nodiscard]] double det() const;

    [[nodiscard]] std::pair<Matrix, Matrix> LU() const;

    [[nodiscard]] Matrix inverse() const;

    bool operator==(const Matrix& mat) const;
    bool operator!=(const Matrix& mat) const;

 private:
    uint32_t _rows;
    uint32_t _cols;
    std::vector<std::vector<double>> _data;
};

Matrix operator*(double val, const Matrix& matrix);
std::ostream& operator<<(std::ostream& os, const Matrix& matrix);

Matrix solve(const Matrix& A, const Matrix& b);

}  // namespace hw
