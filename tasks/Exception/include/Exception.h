#pragma once

#include <exception>
#include <string>

namespace hw {
class Exception : public std::exception {
 public:
    explicit Exception(std::string error);
    [[nodiscard]] const char* what() const noexcept override;

 private:
    std::string _error;
};
}  // namespace hw