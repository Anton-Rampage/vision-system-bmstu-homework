#include "Exception.h"

#include <cstring>

namespace hw {
Exception::Exception(std::string error) : _error(std::move(error)) {
    _error += std::string(" : ") + strerror(errno);
}

const char *Exception::what() const noexcept { return _error.c_str(); }

}  // namespace hw