#include <fstream>
#include <iostream>

#include <Eigen/Core>
#include <Eigen/LU>


template<typename M>
M load_csv(const std::string &path) {
    std::ifstream file(path);
    std::string line;
    std::vector<double> values;
    uint rows = 0;
    // skip headers
    std::getline(file, line);
    // read data
    while (std::getline(file, line)) {
        std::stringstream lineStream(line);
        std::string cell;
        while (std::getline(lineStream, cell, ' ')) {
            values.push_back(std::stod(cell));
        }
        ++rows;
    }
    return Eigen::Map<const Eigen::Matrix<typename M::Scalar, M::RowsAtCompileTime, M::ColsAtCompileTime, Eigen::RowMajor>>(
            values.data(), rows, values.size() / rows);
}

int main() {
    const std::string FILEPATH("../../../tasks/LeastSquares/temp.csv");
    auto data = load_csv<Eigen::MatrixXd>(FILEPATH);
    // create log Z
    Eigen::MatrixXd Z = data.col(0);
    for (long i = 0; i < Z.rows(); ++i) {
        Z(i, 0) = std::log(data(i, 0));
    }
    Eigen::MatrixXd T(data.rows(), 2);
    // append ones col for matrix
    for (long i = 0; i < T.rows(); ++i) {
        T(i, 0) = data(i, 1);
        T(i, 1) = 1;
    }

    auto T_2 = T.transpose() * T;
    auto T_Z = T.transpose() * Z;

    std::cout << T_2.lu().solve(T_Z) << std::endl;
}