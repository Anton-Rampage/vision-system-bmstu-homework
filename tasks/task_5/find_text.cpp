#include <fstream>
#include <opencv2/opencv.hpp>
#include <opencv2/text.hpp>
#include <string>

std::vector<cv::Rect> get_region_of_interest(const cv::Mat& original, const cv::Scalar& lover,
                                             const cv::Scalar& upper) {
    cv::Mat thresholdedMat;
    cv::cvtColor(original, thresholdedMat, cv::COLOR_BGR2HSV_FULL);
    cv::inRange(thresholdedMat, lover, upper, thresholdedMat);
    cv::erode(thresholdedMat, thresholdedMat, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
    cv::dilate(thresholdedMat, thresholdedMat, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
    cv::dilate(thresholdedMat, thresholdedMat, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
    cv::erode(thresholdedMat, thresholdedMat, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5)));
    cv::Canny(thresholdedMat, thresholdedMat, 100, 50, 5);

    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    cv::findContours(thresholdedMat, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
    std::vector<cv::Rect> rects;
    for (uint i = 0; i < contours.size(); ++i) {
        if (hierarchy[i][3] >= 0) {
            continue;
        }
        rects.emplace_back(cv::boundingRect(contours[i]));
    }

    return rects;
}

void detect_text(const std::string& img_filename) {
    cv::Mat orig_img = cv::imread(img_filename);
    auto ocr         = cv::text::OCRTesseract::create(nullptr, nullptr, nullptr, 3, 8);
    std::vector<cv::Mat> res_imgs;
    auto red_regions = get_region_of_interest(orig_img, cv::Scalar(0, 100, 100), cv::Scalar(5, 255, 255));
    for (const auto& red_region : red_regions) {
        cv::Mat red_roi = orig_img(red_region);

        auto black_regions = get_region_of_interest(red_roi, cv::Scalar(0, 0, 0), cv::Scalar(140, 140, 60));
        for (const auto& black_region : black_regions) {
            cv::Mat black_roi = red_roi(black_region);
            cv::Mat small;
            cvtColor(black_roi, small, cv::COLOR_BGR2GRAY);

            cv::Mat grad;
            cv::Mat morphKernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3));
            cv::morphologyEx(small, grad, cv::MORPH_GRADIENT, morphKernel);

            cv::Mat bw;
            cv::threshold(grad, bw, 0.0, 255.0, cv::THRESH_BINARY | cv::THRESH_OTSU);

            cv::Mat connected;
            morphKernel = cv::getStructuringElement(cv::MORPH_RECT, cv::Size(9, 1));
            cv::morphologyEx(bw, connected, cv::MORPH_CLOSE, morphKernel);
            cv::imshow("test", connected);
            cv::waitKey(0);
            cv::Mat mask = cv::Mat::zeros(bw.size(), CV_8UC1);
            std::vector<std::vector<cv::Point>> contours;
            std::vector<cv::Vec4i> hierarchy;
            cv::findContours(connected, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
            if (hierarchy.empty()) {
                continue;
            }

            for (int idx = 0; idx >= 0; idx = hierarchy[idx][0]) {
                cv::Rect rect = cv::boundingRect(contours[idx]);
                cv::Mat maskROI(mask, rect);
                drawContours(mask, contours, idx, cv::Scalar(255, 255, 255), cv::FILLED);
                double r = (double) countNonZero(maskROI) / (rect.width * rect.height);
                if (r > 0.45 && (rect.height > 8 && rect.width > 8)) {
                    rectangle(black_roi, rect, cv::Scalar(0, 255, 0), 2);
                    cv::Size size;
                    if (rect.width * 1.2 + rect.x >= black_roi.cols || rect.height * 1.2 + rect.y >= black_roi.rows) {
                        size = rect.size();
                    } else {
                        size = cv::Size(int(rect.width * 1.2), int(rect.height * 1.2));
                    }
                    auto newRect = cv::Rect({rect.x, rect.y}, size);
                    auto rectImg = black_roi(newRect);
                    std::string output;
                    ocr->run(rectImg, output);
                    output.erase(remove(output.begin(), output.end(), '\n'), output.end());
                    std::cout << output << std::endl;
                    res_imgs.emplace_back(rectImg);
                }
            }
        }
    }
    for (size_t i = 0; i < res_imgs.size(); ++i) {
        cv::imshow("found_" + std::to_string(i), res_imgs[i]);
    }
    cv::imshow("Result", orig_img);
    cv::waitKey(0);
}

int main() {
    detect_text("../../../tasks/task_5/test_marker.jpg");
    return 0;
}