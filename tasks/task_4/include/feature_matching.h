#pragma once

#include <string>

namespace hw {

void feature_matching(const std::string &path, bool enable_adaptive_alignment);

}  // namespace hw
