#include "feature_matching.h"

#include <iostream>
#include <string>

const std::string PATH_TO_VIDEO("../../../tasks/task_4/data/sample_mpg.avi");

int main() {
    std::cout << "Enable adaptive brightness alignment?" << std::endl << "1 - Yes" << std::endl << "0 - No" << std::endl;
    int use = 0;
    std::cin >> use;

    bool enable;
    switch (use) {
        case 1:
            enable = true;
            break;
        case 0:
        default:
            enable = false;
    }

    hw::feature_matching(PATH_TO_VIDEO, enable);

    return 0;
}