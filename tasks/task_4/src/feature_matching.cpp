#include "feature_matching.h"

#include <opencv2/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <tuple>
#include <vector>

namespace hw {

std::pair<cv::Mat, std::vector<cv::KeyPoint>> find_descriptors_with_adaptive_alignment(
    const cv::Mat &frame, const cv::Ptr<cv::FeatureDetector> &detector) {
    cv::Mat gray_frame;
    cv::cvtColor(frame, gray_frame, cv::COLOR_RGB2GRAY);

    cv::Ptr<cv::CLAHE> pClahe = createCLAHE(3.0, cv::Size(8, 8));
    pClahe->setClipLimit(1);

    cv::Mat clahe_frame;
    pClahe->apply(gray_frame, clahe_frame);

    cv::Mat descriptors;
    std::vector<cv::KeyPoint> key_points;

    detector->detectAndCompute(clahe_frame, cv::noArray(), key_points, descriptors);
    return {descriptors, key_points};
}

std::pair<cv::Mat, std::vector<cv::KeyPoint>> find_descriptors_without_adaptive_alignment(
    const cv::Mat &frame, const cv::Ptr<cv::FeatureDetector> &detector) {
    cv::Mat descriptors;
    std::vector<cv::KeyPoint> key_points;

    detector->detectAndCompute(frame, cv::noArray(), key_points, descriptors);

    return {descriptors, key_points};
};

std::pair<cv::Mat, std::vector<cv::KeyPoint>> find_descriptors(const cv::Mat &frame,
                                                               const cv::Ptr<cv::FeatureDetector> &detector,
                                                               bool enable_adaptive_alignment) {
    if (enable_adaptive_alignment) {
        return find_descriptors_with_adaptive_alignment(frame, detector);
    }
    return find_descriptors_without_adaptive_alignment(frame, detector);
}

void feature_matching(const std::string &path, bool enable_adaptive_alignment) {
    cv::Ptr<cv::FeatureDetector> detector = cv::SIFT::create(/*200, 3, 0.05*/);
    cv::BFMatcher matcher;

    cv::Mat image_with_points;

    cv::VideoCapture cap(path);
    if (!cap.isOpened()) {
        throw std::invalid_argument("failed to capturing video file");
    }

    cv::Mat frame;
    cv::Mat prev_frame;

    cap >> prev_frame;
    auto [prev_descriptors, prev_key_points] = find_descriptors(prev_frame, detector, enable_adaptive_alignment);

    while (true) {
        cap >> frame;
        if (frame.empty()) {
            break;
        }

        auto [descriptors, key_points] = find_descriptors(frame, detector, enable_adaptive_alignment);

        std::vector<std::vector<cv::DMatch>> matches;
        matcher.knnMatch(descriptors, prev_descriptors, matches, 2);

        const float ratio_thresh = 0.6f;
        std::vector<cv::DMatch> good_matches;
        for (auto & match : matches) {
            if (match[0].distance < ratio_thresh * match[1].distance) {
                good_matches.push_back(match[0]);
            }
        }
        cv::drawMatches(frame, key_points, prev_frame, prev_key_points, good_matches, image_with_points,
                        cv::Scalar::all(-1), cv::Scalar::all(-1), std::vector<char>(),
                        cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

        cv::imshow(typeid(cv::BRISK).name(), image_with_points);
        cv::imshow("Origin", frame);

        prev_frame       = frame;
        prev_key_points  = key_points;
        prev_descriptors = descriptors;

        if (cv::waitKey(30) == 27) {
            break;
        }
    }

    cap.release();
}
}  // namespace hw