[![pipeline status](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/badges/main/pipeline.svg)](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/commits/main)

# Vision System BMSTU Homework

## Task №1
* [Realization](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tasks/Matrix/)
* [Tests](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tests/gauss_test.cpp)

## Task №2
* [Realization](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tasks/LeastSquares/resistor.cpp)
* [Visualisation](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tasks/LeastSquares/visualisation.ipynb)
* [Data](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tasks/LeastSquares/temp.csv)

## Task №3
* [Realization](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tasks/DiffEquation/)
* [Tests](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tests/diff_eq_test.cpp)

## Task №4
* [Realization](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tasks/task_4/)

## Task №5
* [Realization](https://gitlab.com/Anton-Rampage/vision-system-bmstu-homework/-/tree/main/tasks/task_5/)